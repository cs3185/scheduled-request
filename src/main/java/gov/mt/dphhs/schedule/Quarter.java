package gov.mt.dphhs.schedule;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Quarter {
    private Integer quarter;
    private String calendarType;
    private Date startOfQuarter;
    private Date endOfQuarter;
    private Integer fiscalYear;

    public Integer getQuarter() {
        return quarter;
    }

    public void setQuarter(Integer quarter) {
        this.quarter = quarter;
    }

    public String getCalendarType() {
        return calendarType;
    }

    public void setCalendarType(String calendarType) {
        this.calendarType = calendarType;
    }

    public Date getStartOfQuarter() {
        return startOfQuarter;
    }

    public void setStartOfQuarter(Date startOfQuarter) {
        this.startOfQuarter = startOfQuarter;
    }

    public Date getEndOfQuarter() {
        return endOfQuarter;
    }

    public void setEndOfQuarter(Date endOfQuarter) {
        this.endOfQuarter = endOfQuarter;
    }

    public Integer getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(Integer fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    @Override
    public String toString() {
        String dateFormat = "EEE MMM dd yyyy";
        SimpleDateFormat dateFormater = new SimpleDateFormat(dateFormat);

        return "Quarter{" +
                "quarter=" + quarter +
                ", calendarType='" + calendarType + '\'' +
                ", startOfQuarter=" + dateFormater.format(startOfQuarter) +
                ", endOfQuarter=" + dateFormater.format(endOfQuarter) +
                ", fiscalYear=" + fiscalYear +
                '}';
    }
}
