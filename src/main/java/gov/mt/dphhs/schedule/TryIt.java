package gov.mt.dphhs.schedule;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Component
public class TryIt {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedRate = 60000)
    public void reportCurrentTime() {

        try {
            log.info("The time is now {}", dateFormat.format(new Date()));
            HttpResults dateResults = HttpBuilder.callHttp();
            log.info("Call Date-Service:  {}", dateResults);
            Quarter quarter = new ObjectMapper().readValue(dateResults.getRawData().toString(), Quarter.class);
            log.info("Quarter:  {}", quarter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

