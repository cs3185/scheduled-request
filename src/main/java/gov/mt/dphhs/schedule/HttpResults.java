package gov.mt.dphhs.schedule;

public class HttpResults {
    private Integer statusCode;
    private String statusMessage;
    private StringBuffer rawData;

    public HttpResults() {
        statusCode = 406;
        statusMessage = "Not Accepted.  Request failed.";
        rawData = new StringBuffer();
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public StringBuffer getRawData() {
        return rawData;
    }

    public void setRawData(StringBuffer rawData) {
        this.rawData = rawData;
    }

    public void append(String appendData) {
        this.rawData.append(appendData);
    }

    @Override
    public String toString() {
        return "HttpResults{" +
                "statusCode=" + statusCode +
                ", statusMessage='" + statusMessage + '\'' +
                ", rawData=" + rawData +
                '}';
    }
}
