package gov.mt.dphhs.schedule;


import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;

@Slf4j
public class HttpBuilder {

    private HttpBuilder() {}

    public static HttpResults callHttp() throws IOException {
        HttpResults httpResults = new HttpResults();

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        try {

            URI uri = new URIBuilder()
                    .setScheme("http")
                    .setHost("webservice.hhs.mt.gov")
                    .setPath("/date-service/v1/quarters")
                    .setParameter("calendarType", "state")
                    .build();

            HttpGet httpGet = new HttpGet(uri);

            CloseableHttpResponse response = httpClient.execute(httpGet);

            log.trace("status [{}]  reason: [{}]", response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());

            httpResults.setStatusCode(response.getStatusLine().getStatusCode());
            httpResults.setStatusMessage(response.getStatusLine().getReasonPhrase());

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line;

            while ((line = rd.readLine()) != null) {
                httpResults.append(line);
            }

            log.trace("Data: {}", httpResults.getRawData());

        } catch (Exception ex) {
            log.error("Unexpected Exception when hitting date service", ex);
        } finally {
            httpClient.close();
        }

        return httpResults;
    }
}
